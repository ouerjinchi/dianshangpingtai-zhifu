package com.uzengroup.jkd.portal.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uzengroup.jkd.cache.portal.impl.OrderByIdCacheManager;
import com.uzengroup.jkd.core.Constants;
import com.uzengroup.jkd.core.Constants.PaymentType;
import com.uzengroup.jkd.core.model.Order;
import com.uzengroup.jkd.core.model.WeChatAppConfig;
import com.uzengroup.jkd.core.service.OrderManager;
import com.uzengroup.jkd.core.service.OrderPaymentTypeRecordManager;
import com.uzengroup.jkd.core.util.FileIO;
import com.uzengroup.jkd.core.util.StringUtil;
import com.uzengroup.jkd.core.util.XmlParseUtil;
import com.uzengroup.jkd.core.vo.MsgModel;
import com.uzengroup.jkd.portal.BaseController;
import com.uzengroup.jkd.portal.util.AliPayUtil;
import com.uzengroup.jkd.portal.util.WeiXinPayUtil;

@Controller
@RequestMapping("/api/order/")
public class OrderPayNotifyController extends BaseController{
	// 定义锁对象
	private static Lock lock = new ReentrantLock();
	@Autowired
	private OrderManager orderManager;
	@Autowired
	private OrderByIdCacheManager orderByIdCacheManager;
	@Autowired
	OrderPaymentTypeRecordManager orderPaymentTypeRecordManager;
	
	/**
	 * 更新订单支付成功信息
	 * @param orderId
	 * @param outTradeNo
	 * @param transactionId
	 * @param typeCode
	 * @return
	 */
	public static void updateOrderPaymentSuccStatus(Long orderId, String outTradeNo, String transactionId, Integer paymentType, Long weChatConfigId){
		
	}
	
	/**
	 * 微信支付回调通知
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/wxpayNotify")
	public void wxpayNotify(final HttpServletRequest request, HttpServletResponse response) {
		try{
			String resultXML = FileIO.inputStream2String(request.getInputStream());
			log.info(String.format("weChatPayNotify[XML]==>[%s]", resultXML));
			
			Map<String, Object> notifyObjectMap = XmlParseUtil.xmlCont2Map(resultXML);
			if(notifyObjectMap != null 
					&& notifyObjectMap.size() > 0
					&& notifyObjectMap.containsKey("return_code")
					&& StringUtil.compareObject("SUCCESS", StringUtil.null2Str(notifyObjectMap.get("return_code")))){
				String appid  = StringUtil.null2Str(notifyObjectMap.get("appid"));
				String transactionId = StringUtil.null2Str(notifyObjectMap.get("transaction_id"));
				String outTradeNo = StringUtil.null2Str(notifyObjectMap.get("out_trade_no"));
				String sign = StringUtil.null2Str(notifyObjectMap.get("sign"));
				
                //标识微信支付 1 或者 微信公众号支付 5
				WeChatAppConfig weChatAppConfig = Constants.WECHAT_APP_ID_MAP.get(appid);
				if(weChatAppConfig == null || weChatAppConfig.getAppId() == null){
					this.writeTextResponse(response, "<xml><return_code><![CDATA[FAIL]]></return_code></xml>");
					return;
				}
				
				notifyObjectMap.remove("sign");
				String notifySign = WeiXinPayUtil.getNotifySignString(notifyObjectMap, weChatAppConfig.getSecretKey());
				// 验证签名是否正确
				if(StringUtil.compareObject(notifySign, sign)){
					Order order = this.orderManager.getOrderByOrderNo(outTradeNo);
					if(order != null && order.getOrderId() != null){
						//根据支付流水号获取支付信息
						MsgModel<String> msModel = WeiXinPayUtil.getQueryPayInfo(weChatAppConfig, transactionId, null);
						int orderTotal = WeiXinPayUtil.orderAmountToBranch(order.getOrderAmount());
						if(StringUtil.nullToBoolean(msModel.getIsSucc()) && StringUtil.compareObject(msModel.getData(), orderTotal)){
							OrderPayNotifyController.updateOrderPaymentSuccStatus(order.getOrderId(), outTradeNo, transactionId, PaymentType.PAYMENT_TYPE_WECHAT, weChatAppConfig.getConfigId());
							
							//删除订单对应的支付方式记录
							orderPaymentTypeRecordManager.deleteByOrderId(order.getOrderId());
							this.writeTextResponse(response, "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>");
							return;
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		this.writeTextResponse(response, "<xml><return_code><![CDATA[FAIL]]></return_code></xml>");
	}
	
	/**
	 * 支付宝支付回调通知
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/alipayNotify")
	public void alipayNotify(final HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> params = new HashMap<String, String>(); //将异步通知中收到的待验证所有参数都存放到map中
		Map<String, String[]> requestParams = request.getParameterMap();
		StringBuffer paramBuffer = new StringBuffer();
		if(requestParams != null && requestParams.size() > 0){
			for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
				String name = iter.next();
				String[] values = (String[])requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = i == values.length - 1 ? new StringBuilder().append(valueStr).append(values[i]).toString() : new StringBuilder().append(valueStr).append(values[i]).append(",").toString();
				}

				params.put(name, valueStr);
				paramBuffer.append(new StringBuilder().append(StringUtil.null2Str(name)).append("=").append(StringUtil.null2Str(valueStr)).append(",").toString());
			}
		}

		String out_trade_no = StringUtil.null2Str(params.get("out_trade_no"));
		String trade_no = StringUtil.null2Str(params.get("trade_no"));
		String trade_status = StringUtil.null2Str(params.get("trade_status"));
		log.info(String.format("quickPayment[record]==>[%s]", paramBuffer.toString()));
		
		boolean verify = AliPayUtil.verify(params);
		if(verify){
			try{
				if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
					Order order = this.orderManager.getOrderByOrderNo(out_trade_no);
					if(order != null && order.getOrderId() != null){
						//根据支付流水号获取支付信息
						MsgModel<String> msModel = AliPayUtil.getQueryAliPayInfo(out_trade_no, trade_no);
						if(StringUtil.nullToBoolean(msModel.getIsSucc()) && StringUtil.compareObject(msModel.getData(), StringUtil.formatDouble2Str(order.getOrderAmount()))){
							OrderPayNotifyController.updateOrderPaymentSuccStatus(order.getOrderId(), out_trade_no, trade_no, PaymentType.PAYMENT_TYPE_ALIPAY, null);
							//删除订单对应的支付方式记录
							orderPaymentTypeRecordManager.deleteByOrderId(order.getOrderId());
							this.writeTextResponse(response, "success");
							return;
						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		this.writeTextResponse(response, "fail");
	}
}
